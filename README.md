# OpenML dataset: Edmunds-car-review

https://www.openml.org/d/43348

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Started this for my final year project improved on it during quarantine.
Content
A mix of used and new car reviews from the year 2000 to 2019 of various brands from edmunds.com. I did not  scrape all brands, just the popular ones in the US.
Added a notebook for walkthrough
Inspiration
Did not have a proper car review dataset for my final year project, so I made one

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43348) of an [OpenML dataset](https://www.openml.org/d/43348). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43348/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43348/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43348/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

